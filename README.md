Debian DNS Team
===============

This repo collects documentation for people doing
packaging, system integration work, and maintenance work
on DNS-related software in [Debian](https://debian.org).

Communications
--------------

- This [collection of documentation](https://salsa.debian.org/dns-team/debian-dns-docs)
- Internet Relay Chat, at `#debian-dns` on `irc.oftc.net`
- We currently have no mailing list

Packages
--------

- bind9
- dns-root-data
- getdns
- knot
- knot-resolver
- nsd
- powerdns
- unbound

System Integration
-------------------

- resolvconf
- openresolv
- resolvconf-admin
- libc6 (gethostbyname, etc)
- systemd (systemd-resolved)
- network-manager
- dnssec-trigger
- Web Browsers (DNS-over-HTTPS?)

People
------

Some of the people contributing to this team can be found on the [dns-team members page](https://salsa.debian.org/groups/dns-team/-/group_members).

Please request membership if you want to do this work!
Gitlab doesn't give a chance to really explain who you are or why you want to join, so please come chat with folks on IRC if no one acts on it.
